/* bit-guess.h */
/*
    Copyright (C) 2018 bg nerilex (bg@nerilex.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BIT_GUESS_H_
#define BIT_GUESS_H_

#include <stddef.h>
typedef size_t index_t;
typedef unsigned char bit_t;

typedef index_t select_index_func(const bit_t[], index_t max);


#endif /* BIT_GUESS_H_ */
